﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo;

namespace Controlador
{
    public class ClNomina
    {
        //metod en el con trolador para consultar sueldos 
        public Nomina consultarSueldo(string numero, DateTime fecha )
        {
            MDNomina nom = new MDNomina();
            Nomina empnom = new Nomina();
            string Fecha = fecha.ToString();
            DateTime fecha1;
            DateTime fecha2;
            fecha1 = Convert.ToDateTime( "01/" + Fecha.Substring(3, 7));
            fecha2 = Convert.ToDateTime( "30/" + Fecha.Substring(3, 7));
            empnom = nom.consultarSueldo(numero, fecha1, fecha2);
            return empnom;
        }
    }
}
