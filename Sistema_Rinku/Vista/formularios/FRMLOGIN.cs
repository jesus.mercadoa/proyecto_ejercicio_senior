﻿using Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Vista.Formularios
{
    public partial class FRMLOGIN : Form
    {
        public static string USUARIO;
        public static string NIVEL;
        string sconexion;

       
        public FRMLOGIN()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void txtusuario_KeyPress(object sender, KeyPressEventArgs e)
        {
           
           //metodo en evento para consultar usuario para el acceso al sistema
            if (e.KeyChar == (char)13)
            {
               ClLogin l = new ClLogin(txtusuario.Text);
                ClConexion c = new ClConexion(l.consultarusuario());
                DataSet ds = new DataSet();
                ds = c.Consultar();
                if (ds.Tables["Tabla"].Rows.Count >0)
                {
                    txtcontrasena.Focus();
                    txtcontrasena.Enabled = true;
                    txtusuario.Enabled = false;
                }
                else
                {
                    MessageBox.Show("El usuario no existe", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtusuario.Clear();
                }
               
               
            }

            
        }

        private void FRMLOGIN_Load(object sender, EventArgs e)
        {
            txtcontrasena.Enabled = false;
            txtusuario.Focus();
        }

        private void txtcontrasena_KeyPress(object sender, KeyPressEventArgs e)
        {
            //metodo para consultar contrasena para el acceso del sistema
            if (e.KeyChar == (char)13)
            {
                ClLogin l = new ClLogin(txtusuario.Text, txtcontrasena.Text);
                ClConexion c = new ClConexion(l.consultarusuarios());
                DataSet ds = new DataSet();
                ds = c.Consultar();
                if (ds.Tables["Tabla"].Rows.Count > 0)
                {
                   
                   
                    string usuario = ds.Tables["Tabla"].Rows[0]["Usuario"].ToString();
                    string nivel1 = ds.Tables["Tabla"].Rows[0]["Nivel"].ToString();
                    string nivel2 ="";
                    switch (nivel1)
                    {
                        case "1":
                            nivel2 = "Administrador";
                            break;
                        case "2":
                            nivel2 = "Operador";
                            break;
                        case "3":
                            nivel2 = "Visita";
                            break;
                           
                        }
                    FRMMenuGeneral m = new FRMMenuGeneral(nivel1);
                    m.Text = "*| MENU GENERAL | SISTEMA CONTROL EMPLEADOS RINKU | " + "Usuario: "+ usuario +" | Nivel: "+ nivel2 +" |*" ;
                    m.ForeColor = Color.Black;
                    this.Hide();
                    m.ShowDialog();
                    this.Close();
                   
                    
                }
                else
                {
                    MessageBox.Show("La contraseña es incorrecta", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtcontrasena.Clear();
                }
                

            }
        }
        
        
    }
}
