﻿using Controlador;
using Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vista.Formularios
{
    public partial class FRMChecador : Form
    {
        CLValidaciones v = new CLValidaciones();
        int opcion;
        public FRMChecador()
        {
            InitializeComponent();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if(rdbe.Checked == true || rdbs.Checked == true)
                { 
                    //metodo opara consultar el colaborador que esta checando engrada o salida
                    ClEmpleados emp = new ClEmpleados();
                    Empleado empleado = new Empleado();

                    empleado = emp.consultarByNumeroEmpleado(1, txtnumero.Text);
                    int estatus = empleado.Getstatus();
                    if (estatus == 1)
                    {
                        lblnombre.Text = empleado.Getnombre() + " " + empleado.Getapaterno() + " " + empleado.Getamaterno();

                        int rol = empleado.Getrol();
                        int tipo = empleado.Gettipo();
                        switch (rol)
                        {
                            case 1:
                                lblrol.Text = "Chofer";
                                break;
                            case 2:
                                lblrol.Text = "Cargador";
                                break;
                            case 3:
                                lblrol.Text = "Auxiliar";
                                break;

                        }
                        switch (tipo)
                        {
                            case 1:
                                lbltipo.Text = "Interno";
                                break;
                            case 2:
                                lblrol.Text = "Externo";
                                break;

                        }
                        //metodo que relaiza el graabado d elas entradas y salidad
                        CLChecador check = new CLChecador();
                        int res = check.grabar(txtnumero.Text, dtpfecha.Text, dtptiempo.Text, opcion);
                        if (res == 1)
                        {
                            pbcheck.Image = new System.Drawing.Bitmap(@"C:\Ejercicio_senior\proyecto_ejercicio_senior\Sistema_Rinku\Vista\Imagenes\check.jpg");
                        }
                        else
                        {
                            pbcheck.Image = new System.Drawing.Bitmap(@"C:\Ejercicio_senior\proyecto_ejercicio_senior\Sistema_Rinku\Vista\Imagenes\uncheck.jpg");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Empleado no existe ", "mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        pbcheck.Image = new System.Drawing.Bitmap(@"C:\Ejercicio_senior\proyecto_ejercicio_senior\Sistema_Rinku\Vista\Imagenes\uncheck.jpg");
                        lblnombre.Text = "";
                        lbltipo.Text = "";
                        lblrol.Text = "";
                    }

                }
                else
                {
                    pbcheck.Image = new System.Drawing.Bitmap(@"C:\Ejercicio_senior\proyecto_ejercicio_senior\Sistema_Rinku\Vista\Imagenes\uncheck.jpg");
                    lblnombre.Text = "";
                    lbltipo.Text = "";
                    lblrol.Text = "";
                }
            }
            else
            {
                v.solonumeros(e);
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            opcion = 1;
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            opcion = 2;
        }

        private void FRMChecador_Load(object sender, EventArgs e)
        {
            lblnombre.Text = "";
            lbltipo.Text = "";
            lblrol.Text = "";
        }
    }
}
