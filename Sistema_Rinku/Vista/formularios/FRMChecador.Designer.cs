﻿namespace Vista.Formularios
{
    partial class FRMChecador
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtnumero = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblnombre = new System.Windows.Forms.Label();
            this.lblrol = new System.Windows.Forms.Label();
            this.lbltipo = new System.Windows.Forms.Label();
            this.dtpfecha = new System.Windows.Forms.DateTimePicker();
            this.dtptiempo = new System.Windows.Forms.DateTimePicker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdbe = new System.Windows.Forms.RadioButton();
            this.rdbs = new System.Windows.Forms.RadioButton();
            this.pbcheck = new System.Windows.Forms.PictureBox();
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbcheck)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.pbcheck);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.txtnumero);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(29, 44);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(330, 394);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(103, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Numero de empleado";
            // 
            // txtnumero
            // 
            this.txtnumero.Location = new System.Drawing.Point(101, 66);
            this.txtnumero.Name = "txtnumero";
            this.txtnumero.Size = new System.Drawing.Size(100, 20);
            this.txtnumero.TabIndex = 1;
            this.txtnumero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.lbltipo);
            this.groupBox1.Controls.Add(this.lblrol);
            this.groupBox1.Controls.Add(this.lblnombre);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(17, 86);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(299, 100);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nombre:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 50);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(26, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Rol:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 74);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(28, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Tipo";
            // 
            // lblnombre
            // 
            this.lblnombre.AutoSize = true;
            this.lblnombre.Location = new System.Drawing.Point(67, 25);
            this.lblnombre.Name = "lblnombre";
            this.lblnombre.Size = new System.Drawing.Size(35, 13);
            this.lblnombre.TabIndex = 4;
            this.lblnombre.Text = "label5";
            // 
            // lblrol
            // 
            this.lblrol.AutoSize = true;
            this.lblrol.Location = new System.Drawing.Point(67, 50);
            this.lblrol.Name = "lblrol";
            this.lblrol.Size = new System.Drawing.Size(35, 13);
            this.lblrol.TabIndex = 5;
            this.lblrol.Text = "label6";
            // 
            // lbltipo
            // 
            this.lbltipo.AutoSize = true;
            this.lbltipo.Location = new System.Drawing.Point(67, 74);
            this.lbltipo.Name = "lbltipo";
            this.lbltipo.Size = new System.Drawing.Size(35, 13);
            this.lbltipo.TabIndex = 6;
            this.lbltipo.Text = "label7";
            // 
            // dtpfecha
            // 
            this.dtpfecha.Enabled = false;
            this.dtpfecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpfecha.Location = new System.Drawing.Point(29, 18);
            this.dtpfecha.Name = "dtpfecha";
            this.dtpfecha.Size = new System.Drawing.Size(80, 20);
            this.dtpfecha.TabIndex = 1;
            // 
            // dtptiempo
            // 
            this.dtptiempo.Enabled = false;
            this.dtptiempo.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtptiempo.Location = new System.Drawing.Point(116, 18);
            this.dtptiempo.Name = "dtptiempo";
            this.dtptiempo.Size = new System.Drawing.Size(63, 20);
            this.dtptiempo.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdbs);
            this.groupBox2.Controls.Add(this.rdbe);
            this.groupBox2.Location = new System.Drawing.Point(17, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(291, 45);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            // 
            // rdbe
            // 
            this.rdbe.AutoSize = true;
            this.rdbe.Location = new System.Drawing.Point(65, 19);
            this.rdbe.Name = "rdbe";
            this.rdbe.Size = new System.Drawing.Size(62, 17);
            this.rdbe.TabIndex = 0;
            this.rdbe.TabStop = true;
            this.rdbe.Text = "Entrada";
            this.rdbe.UseVisualStyleBackColor = true;
            this.rdbe.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // rdbs
            // 
            this.rdbs.AutoSize = true;
            this.rdbs.Location = new System.Drawing.Point(170, 19);
            this.rdbs.Name = "rdbs";
            this.rdbs.Size = new System.Drawing.Size(54, 17);
            this.rdbs.TabIndex = 1;
            this.rdbs.TabStop = true;
            this.rdbs.Text = "Salida";
            this.rdbs.UseVisualStyleBackColor = true;
            this.rdbs.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // pbcheck
            // 
            this.pbcheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pbcheck.Location = new System.Drawing.Point(73, 217);
            this.pbcheck.Name = "pbcheck";
            this.pbcheck.Size = new System.Drawing.Size(182, 141);
            this.pbcheck.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbcheck.TabIndex = 3;
            this.pbcheck.TabStop = false;
            // 
            // FRMChecador
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(399, 450);
            this.Controls.Add(this.dtptiempo);
            this.Controls.Add(this.dtpfecha);
            this.Controls.Add(this.panel1);
            this.Name = "FRMChecador";
            this.Text = "Checador";
            this.Load += new System.EventHandler(this.FRMChecador_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbcheck)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pbcheck;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lbltipo;
        private System.Windows.Forms.Label lblrol;
        private System.Windows.Forms.Label lblnombre;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtnumero;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpfecha;
        private System.Windows.Forms.DateTimePicker dtptiempo;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdbs;
        private System.Windows.Forms.RadioButton rdbe;
    }
}