﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vista.Formularios
{
    public partial class FRMMenuGeneral : Form
    {
        string nivel;
        public FRMMenuGeneral(string nivel1)
        {
            InitializeComponent();
            this.nivel = nivel1;
        }
        //muestra los accesos a los diferentes apartados en el sistema 
        private void eMPLEADOSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Formularios.FRMEmpleados EMP = new FRMEmpleados();
            EMP.MdiParent = this;
            EMP.Show();
        }

        private void mOVIMIENTOSToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Formularios.FRMMovimientos mov = new FRMMovimientos();
            mov.MdiParent = this;
            mov.Show();
        }

        private void nOMINAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Formularios.FRMSaldos s = new FRMSaldos();
            s.MdiParent = this;
            s.Show();

        }

        private void cHECADORToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Formularios.FRMChecador ch = new FRMChecador();
            ch.MdiParent = this;
            ch.Show();
        }

        private void FRMMenuGeneral_Load(object sender, EventArgs e)
        {
            
            if (nivel == "1")
            {
                btnempelados.Visible = true;
                btnchecador.Visible = true;
                btnmovimientos.Visible = true;
                btnnomina.Visible = true;
            }
            else
            {
                btnempelados.Visible = false;
                btnchecador.Visible = true;
                btnmovimientos.Visible = false;
                btnnomina.Visible = false;
            }
        }
    }
}
