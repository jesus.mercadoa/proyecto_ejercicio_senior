﻿namespace Vista.Formularios
{
    partial class FRMEmpleados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdbauxiliar = new System.Windows.Forms.RadioButton();
            this.rdbcargador = new System.Windows.Forms.RadioButton();
            this.rdbchofer = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdbexterno = new System.Windows.Forms.RadioButton();
            this.rdbinterno = new System.Windows.Forms.RadioButton();
            this.txtnumero = new System.Windows.Forms.TextBox();
            this.txtnombre = new System.Windows.Forms.TextBox();
            this.txtapaterno = new System.Windows.Forms.TextBox();
            this.txtamaterno = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dtgEmpleados = new System.Windows.Forms.DataGridView();
            this.Numero = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Rol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tipo1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btneliminar = new System.Windows.Forms.Button();
            this.btnbuscar = new System.Windows.Forms.Button();
            this.btngrabar = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtgEmpleados)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(108, 49);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Numero:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(106, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nombre";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(66, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(86, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Apellido paterno:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(65, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Apellido materno";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdbauxiliar);
            this.groupBox1.Controls.Add(this.rdbcargador);
            this.groupBox1.Controls.Add(this.rdbchofer);
            this.groupBox1.Location = new System.Drawing.Point(64, 175);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(88, 100);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Rol";
            // 
            // rdbauxiliar
            // 
            this.rdbauxiliar.AutoSize = true;
            this.rdbauxiliar.Location = new System.Drawing.Point(6, 65);
            this.rdbauxiliar.Name = "rdbauxiliar";
            this.rdbauxiliar.Size = new System.Drawing.Size(58, 17);
            this.rdbauxiliar.TabIndex = 2;
            this.rdbauxiliar.TabStop = true;
            this.rdbauxiliar.Text = "Auxiliar";
            this.rdbauxiliar.UseVisualStyleBackColor = true;
            this.rdbauxiliar.CheckedChanged += new System.EventHandler(this.rdbauxiliar_CheckedChanged);
            // 
            // rdbcargador
            // 
            this.rdbcargador.AutoSize = true;
            this.rdbcargador.Location = new System.Drawing.Point(6, 42);
            this.rdbcargador.Name = "rdbcargador";
            this.rdbcargador.Size = new System.Drawing.Size(68, 17);
            this.rdbcargador.TabIndex = 1;
            this.rdbcargador.TabStop = true;
            this.rdbcargador.Text = "Cargador";
            this.rdbcargador.UseVisualStyleBackColor = true;
            this.rdbcargador.CheckedChanged += new System.EventHandler(this.rdbcargador_CheckedChanged);
            // 
            // rdbchofer
            // 
            this.rdbchofer.AutoSize = true;
            this.rdbchofer.Location = new System.Drawing.Point(6, 19);
            this.rdbchofer.Name = "rdbchofer";
            this.rdbchofer.Size = new System.Drawing.Size(56, 17);
            this.rdbchofer.TabIndex = 0;
            this.rdbchofer.TabStop = true;
            this.rdbchofer.Text = "Chofer";
            this.rdbchofer.UseVisualStyleBackColor = true;
            this.rdbchofer.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdbexterno);
            this.groupBox2.Controls.Add(this.rdbinterno);
            this.groupBox2.Location = new System.Drawing.Point(170, 175);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(86, 100);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tipo";
            // 
            // rdbexterno
            // 
            this.rdbexterno.AutoSize = true;
            this.rdbexterno.Location = new System.Drawing.Point(6, 52);
            this.rdbexterno.Name = "rdbexterno";
            this.rdbexterno.Size = new System.Drawing.Size(61, 17);
            this.rdbexterno.TabIndex = 4;
            this.rdbexterno.TabStop = true;
            this.rdbexterno.Text = "Externo";
            this.rdbexterno.UseVisualStyleBackColor = true;
            this.rdbexterno.CheckedChanged += new System.EventHandler(this.rdbexterno_CheckedChanged);
            // 
            // rdbinterno
            // 
            this.rdbinterno.AutoSize = true;
            this.rdbinterno.Location = new System.Drawing.Point(6, 29);
            this.rdbinterno.Name = "rdbinterno";
            this.rdbinterno.Size = new System.Drawing.Size(58, 17);
            this.rdbinterno.TabIndex = 3;
            this.rdbinterno.TabStop = true;
            this.rdbinterno.Text = "Interno";
            this.rdbinterno.UseVisualStyleBackColor = true;
            this.rdbinterno.CheckedChanged += new System.EventHandler(this.rdbinterno_CheckedChanged);
            // 
            // txtnumero
            // 
            this.txtnumero.Location = new System.Drawing.Point(156, 46);
            this.txtnumero.MaxLength = 8;
            this.txtnumero.Name = "txtnumero";
            this.txtnumero.Size = new System.Drawing.Size(57, 20);
            this.txtnumero.TabIndex = 6;
            this.txtnumero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnumero_KeyPress);
            // 
            // txtnombre
            // 
            this.txtnombre.Location = new System.Drawing.Point(156, 76);
            this.txtnombre.Name = "txtnombre";
            this.txtnombre.Size = new System.Drawing.Size(100, 20);
            this.txtnombre.TabIndex = 7;
            this.txtnombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnombre_KeyPress);
            // 
            // txtapaterno
            // 
            this.txtapaterno.Location = new System.Drawing.Point(156, 106);
            this.txtapaterno.Name = "txtapaterno";
            this.txtapaterno.Size = new System.Drawing.Size(100, 20);
            this.txtapaterno.TabIndex = 8;
            this.txtapaterno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtapaterno_KeyPress);
            // 
            // txtamaterno
            // 
            this.txtamaterno.Location = new System.Drawing.Point(156, 134);
            this.txtamaterno.Name = "txtamaterno";
            this.txtamaterno.Size = new System.Drawing.Size(100, 20);
            this.txtamaterno.TabIndex = 9;
            this.txtamaterno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtamaterno_KeyPress);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.btneliminar);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnbuscar);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.btngrabar);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.txtamaterno);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtapaterno);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.txtnombre);
            this.panel1.Controls.Add(this.txtnumero);
            this.panel1.Location = new System.Drawing.Point(21, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(356, 387);
            this.panel1.TabIndex = 13;
            // 
            // label7
            // 
            this.label7.AccessibleRole = System.Windows.Forms.AccessibleRole.OutlineButton;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(234, 314);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Eliminar";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(144, 314);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "Buscar";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(54, 314);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Grabar";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel2.Controls.Add(this.dtgEmpleados);
            this.panel2.Location = new System.Drawing.Point(412, 27);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(481, 384);
            this.panel2.TabIndex = 14;
            // 
            // dtgEmpleados
            // 
            this.dtgEmpleados.AllowUserToAddRows = false;
            this.dtgEmpleados.AllowUserToDeleteRows = false;
            this.dtgEmpleados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgEmpleados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Numero,
            this.Nombre,
            this.Rol1,
            this.Tipo1});
            this.dtgEmpleados.Location = new System.Drawing.Point(19, 24);
            this.dtgEmpleados.Name = "dtgEmpleados";
            this.dtgEmpleados.ReadOnly = true;
            this.dtgEmpleados.Size = new System.Drawing.Size(439, 334);
            this.dtgEmpleados.TabIndex = 0;
            // 
            // Numero
            // 
            this.Numero.HeaderText = "Numero";
            this.Numero.Name = "Numero";
            this.Numero.ReadOnly = true;
            this.Numero.Width = 70;
            // 
            // Nombre
            // 
            this.Nombre.HeaderText = "Nombre";
            this.Nombre.Name = "Nombre";
            this.Nombre.ReadOnly = true;
            this.Nombre.Width = 200;
            // 
            // Rol1
            // 
            this.Rol1.HeaderText = "Rol";
            this.Rol1.Name = "Rol1";
            this.Rol1.ReadOnly = true;
            this.Rol1.Width = 70;
            // 
            // Tipo1
            // 
            this.Tipo1.HeaderText = "Tipo";
            this.Tipo1.Name = "Tipo1";
            this.Tipo1.ReadOnly = true;
            this.Tipo1.Width = 70;
            // 
            // btneliminar
            // 
            this.btneliminar.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btneliminar.BackgroundImage = global::Vista.Properties.Resources.depositphotos_27326509_Delete_icon;
            this.btneliminar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btneliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btneliminar.Location = new System.Drawing.Point(212, 281);
            this.btneliminar.Name = "btneliminar";
            this.btneliminar.Size = new System.Drawing.Size(80, 34);
            this.btneliminar.TabIndex = 12;
            this.btneliminar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btneliminar.UseVisualStyleBackColor = false;
            this.btneliminar.Click += new System.EventHandler(this.btneliminar_Click);
            // 
            // btnbuscar
            // 
            this.btnbuscar.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btnbuscar.BackgroundImage = global::Vista.Properties.Resources.hombre_de_negocios_de_papel_de_la_solicitud_de_un_puesto_de_trabajo_318_46623;
            this.btnbuscar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnbuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnbuscar.Location = new System.Drawing.Point(120, 281);
            this.btnbuscar.Name = "btnbuscar";
            this.btnbuscar.Size = new System.Drawing.Size(86, 34);
            this.btnbuscar.TabIndex = 11;
            this.btnbuscar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnbuscar.UseVisualStyleBackColor = false;
            this.btnbuscar.Click += new System.EventHandler(this.btnbuscar_Click);
            // 
            // btngrabar
            // 
            this.btngrabar.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.btngrabar.BackgroundImage = global::Vista.Properties.Resources.guardar;
            this.btngrabar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btngrabar.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btngrabar.Location = new System.Drawing.Point(28, 281);
            this.btngrabar.Name = "btngrabar";
            this.btngrabar.Size = new System.Drawing.Size(86, 34);
            this.btngrabar.TabIndex = 10;
            this.btngrabar.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btngrabar.UseVisualStyleBackColor = false;
            this.btngrabar.Click += new System.EventHandler(this.btngrabar_Click);
            // 
            // FRMEmpleados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(907, 438);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "FRMEmpleados";
            this.Text = "FRMEmpleados";
            this.Load += new System.EventHandler(this.FRMEmpleados_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dtgEmpleados)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdbauxiliar;
        private System.Windows.Forms.RadioButton rdbcargador;
        private System.Windows.Forms.RadioButton rdbchofer;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdbexterno;
        private System.Windows.Forms.RadioButton rdbinterno;
        private System.Windows.Forms.TextBox txtnumero;
        private System.Windows.Forms.TextBox txtnombre;
        private System.Windows.Forms.TextBox txtapaterno;
        private System.Windows.Forms.TextBox txtamaterno;
        private System.Windows.Forms.Button btngrabar;
        private System.Windows.Forms.Button btnbuscar;
        private System.Windows.Forms.Button btneliminar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dtgEmpleados;
        private System.Windows.Forms.DataGridViewTextBoxColumn Numero;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn Rol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tipo1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
    }
}