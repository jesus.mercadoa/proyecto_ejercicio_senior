﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Controlador;
using Modelo;

namespace Vista.Formularios
{
    public partial class FRMSaldos : Form
    {
        CLValidaciones v = new CLValidaciones();
        public FRMSaldos()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //consulta el emplado para la nomina 
            ClEmpleados emp = new ClEmpleados();
            Empleado empleado = new Empleado();

            empleado = emp.consultarByNumeroEmpleado(1, txtnumero.Text);
            int estatus = empleado.Getstatus();
            if (estatus == 1)
            {
                lblnombre.Text = empleado.Getnombre() + " " + empleado.Getapaterno() + " " + empleado.Getamaterno();

                int rol = empleado.Getrol();
                int tipo = empleado.Gettipo();
                switch (rol)
                {
                    case 1:
                        lblrol.Text = "Chofer";
                        break;
                    case 2:
                        lblrol.Text = "Cargador";
                        break;
                    case 3:
                        lblrol.Text = "Auxiliar";
                        break;

                }
                switch (tipo)
                {
                    case 1:
                        lbltipo.Text = "Interno";
                        break;
                    case 2:
                        lblrol.Text = "Externo";
                        break;

                }
                //genera y obtiene los pagos y conceptos de la nomina
                ClNomina nom = new ClNomina();
                Nomina empnom = new Nomina();
                empnom = nom.consultarSueldo(txtnumero.Text, dtpfecha.Value);
                double sueldobasedia = 30 * 8;
                double sueldobasemes = sueldobasedia * 30;
                double sueldomensual;

                double bono = empnom.Getbonos();
                double entregas = empnom.Getpago_horas();
                sueldomensual = bono + entregas + sueldobasemes;
                double vale = sueldomensual * 0.04;
                sueldomensual = sueldomensual + vale;
                double isr;
                double totalingreso = sueldobasemes+bono+entregas+vale;
                double adicisr = 16000.00;
                if (sueldomensual > adicisr)
                {
                   isr = sueldomensual * 0.12;
                }
                else
                {
                    isr = sueldomensual * 0.09;
                }
                double totalapagar = totalingreso - isr;
                lblsueldo.Text =sueldobasemes.ToString("n");
                lblbonos.Text = bono.ToString("n");
                lblproduct.Text = entregas.ToString("n");
                lblvale.Text = vale.ToString("n");
                lblisr.Text = isr.ToString("n");
                lbltotaling.Text = totalingreso.ToString("n");
                lbltotalegre.Text = isr.ToString("n");
                lbltotalapagar.Text = totalapagar.ToString("n");
     
            }
            else
            {
                MessageBox.Show("Empleado no existe ", "mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
               
            }
            
           
        }

        

        private void txtnumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                
            }
            else
            {
                v.solonumeros(e);
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }
    }
}
