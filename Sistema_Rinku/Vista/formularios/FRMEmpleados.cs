﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Controlador;
using Modelo;
namespace Vista.Formularios
{
    public partial class FRMEmpleados : Form
    {
        int rol;
        int tipo;
        CLValidaciones v = new CLValidaciones();
        public FRMEmpleados()
        {
            InitializeComponent();
        }

        private void FRMEmpleados_Load(object sender, EventArgs e)
        {
            rdbchofer.Checked = false;
            rdbcargador.Checked = false;
            rdbauxiliar.Checked = false;
            rdbinterno.Checked = false;
            rdbexterno.Checked = false;
            cargarEmpleados();

        }
        public void cargarEmpleados()
        {
            //se crea metodo para obtener los empleados registrados y mostrarlos en el grid
            string rol = "";
            string tipo = "";
            string numero = "";
            string nombre = "";
            dtgEmpleados.Rows.Clear();
            ClEmpleados emp = new ClEmpleados();
            ArrayList lista = emp.consultarEmpleados();
            foreach (Empleado empleado in lista)
            {
                numero = empleado.Getnumero();
                nombre = empleado.Getnombre() + " " + empleado.Getapaterno() + " " + empleado.Getamaterno();

                switch (empleado.Getrol())
                {
                    case 1:
                        rol = "Chofer";
                        break;
                    case 2:
                        rol = "Cargador";
                        break;
                    case 3:
                        rol = "Auxiliar";
                        break;
                }
                switch (empleado.Gettipo())
                {
                    case 1:
                        tipo = "Interno";
                        break;
                    case 2:
                        tipo = "Externo";
                        break;
                }
                dtgEmpleados.Rows.Insert(0, numero, nombre, rol, tipo);


            }
        }
        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            rol = 1;
        }

        private void rdbcargador_CheckedChanged(object sender, EventArgs e)
        {
            rol = 2;
        }

        private void rdbauxiliar_CheckedChanged(object sender, EventArgs e)
        {
            rol = 3;
        }

        private void rdbinterno_CheckedChanged(object sender, EventArgs e)
        {
            tipo = 1;
        }

        private void rdbexterno_CheckedChanged(object sender, EventArgs e)
        {
            tipo = 2;
        }

        private void btngrabar_Click(object sender, EventArgs e)
        {
            //graba y modifica empleados
            ClEmpleados emp = new ClEmpleados();
            Empleado empleado = new Empleado();
            empleado.Setnumero(txtnumero.Text);
            empleado.Setnombre(txtnombre.Text);
            empleado.Setapaterno(txtapaterno.Text);
            empleado.Setamaterno(txtamaterno.Text);
            empleado.Setrol(rol);
            empleado.Settipo(tipo);
            empleado.Setstatus(1);
            empleado.Setopcion(2);
            MessageBox.Show(emp.grabar(empleado), "mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            txtnumero.Text = "";
            txtnombre.Text = "";
            txtapaterno.Text = "";
            txtamaterno.Text = "";
            rdbchofer.Checked = false;
            rdbcargador.Checked = false;
            rdbauxiliar.Checked = false;
            rdbinterno.Checked = false;
            rdbexterno.Checked = false;
            cargarEmpleados();
        }

        private void btnbuscar_Click(object sender, EventArgs e)
        {
            //metodo para consultar empleados por su numero 
            ClEmpleados emp = new ClEmpleados();
            Empleado empleado = new Empleado();
            
           
            empleado = emp.consultarByNumeroEmpleado(1,txtnumero.Text);
            int estatus = empleado.Getstatus();
            if (estatus == 1)
            {
                txtnombre.Text = empleado.Getnombre();
                txtapaterno.Text = empleado.Getapaterno();
                txtamaterno.Text = empleado.Getamaterno();
                int rol = empleado.Getrol();
                int tipo = empleado.Gettipo();
                switch (rol)
                {
                    case 1:
                        rdbchofer.Checked = true;
                        break;
                    case 2:
                        rdbcargador.Checked = true;
                        break;
                    case 3:
                        rdbauxiliar.Checked = true;
                        break;

                }
                switch (tipo)
                {
                    case 1:
                        rdbinterno.Checked = true;
                        break;
                    case 2:
                        rdbexterno.Checked = true;
                        break;

                }
            }
            else
            {
                MessageBox.Show("Empleado no existe ", "mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }
        }

        private void txtnumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                txtnombre.Focus();
            }
            else
            {
                v.solonumeros(e);
            }
        }

        private void txtnombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                txtapaterno.Focus();
            }
            else
            {
                v.sololetras(e);
            }
        }

        private void txtapaterno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                txtamaterno.Focus();
            }
            else
            {
                v.sololetras(e);
            }
        }

        private void txtamaterno_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                txtamaterno.Focus();
            }
            else
            {
                v.sololetras(e);
            }
        }

        private void btneliminar_Click(object sender, EventArgs e)
        {
            //metodo para eliminar empleados
            if (txtnumero.Text == "")
            {

                MessageBox.Show(" Por favor verifique que existan datos validos para eliminar un registro en Empleados ", "Excepcion!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (MessageBox.Show("Se eliminara Empleado de los registros ¿Esta seguro de realizar esta acción?", "Salir", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                {
                    ClEmpleados emp = new ClEmpleados();
                    MessageBox.Show(emp.eliminar(txtnumero.Text), "mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    txtnumero.Text = "";
                    txtnombre.Text = "";
                    txtapaterno.Text = "";
                    txtamaterno.Text = "";
                    rdbchofer.Checked = false;
                    rdbcargador.Checked = false;
                    rdbauxiliar.Checked = false;
                    rdbinterno.Checked = false;
                    rdbexterno.Checked = false;
                    cargarEmpleados();
                }


            }
            
        }
    }
}
