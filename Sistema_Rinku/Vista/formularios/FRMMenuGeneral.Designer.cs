﻿namespace Vista.Formularios
{
    partial class FRMMenuGeneral
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.btnempelados = new System.Windows.Forms.ToolStripMenuItem();
            this.btnmovimientos = new System.Windows.Forms.ToolStripMenuItem();
            this.btnnomina = new System.Windows.Forms.ToolStripMenuItem();
            this.btnchecador = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnempelados,
            this.btnmovimientos,
            this.btnnomina,
            this.btnchecador});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1148, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // btnempelados
            // 
            this.btnempelados.Name = "btnempelados";
            this.btnempelados.Size = new System.Drawing.Size(86, 20);
            this.btnempelados.Text = "EMPLEADOS";
            this.btnempelados.Click += new System.EventHandler(this.eMPLEADOSToolStripMenuItem_Click);
            // 
            // btnmovimientos
            // 
            this.btnmovimientos.Name = "btnmovimientos";
            this.btnmovimientos.Size = new System.Drawing.Size(99, 20);
            this.btnmovimientos.Text = "MOVIMIENTOS";
            this.btnmovimientos.Click += new System.EventHandler(this.mOVIMIENTOSToolStripMenuItem_Click);
            // 
            // btnnomina
            // 
            this.btnnomina.Name = "btnnomina";
            this.btnnomina.Size = new System.Drawing.Size(68, 20);
            this.btnnomina.Text = "NOMINA";
            this.btnnomina.Click += new System.EventHandler(this.nOMINAToolStripMenuItem_Click);
            // 
            // btnchecador
            // 
            this.btnchecador.Name = "btnchecador";
            this.btnchecador.Size = new System.Drawing.Size(82, 20);
            this.btnchecador.Text = "CHECADOR";
            this.btnchecador.Click += new System.EventHandler(this.cHECADORToolStripMenuItem_Click);
            // 
            // FRMMenuGeneral
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1148, 578);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.Name = "FRMMenuGeneral";
            this.Text = "FRMMenuGeneral";
            this.Load += new System.EventHandler(this.FRMMenuGeneral_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem btnempelados;
        private System.Windows.Forms.ToolStripMenuItem btnmovimientos;
        private System.Windows.Forms.ToolStripMenuItem btnnomina;
        private System.Windows.Forms.ToolStripMenuItem btnchecador;
    }
}