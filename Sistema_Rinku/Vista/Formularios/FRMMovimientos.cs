﻿using Controlador;
using Modelo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Vista.Formularios
{
    public partial class FRMMovimientos : Form
    {
        CLValidaciones v = new CLValidaciones();
        int turno = 0;
        int rol_cubrio = 0;
        public FRMMovimientos()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtnumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                //metodo que consulta el empleado en el evento d ela caja de texto
                ClEmpleados emp = new ClEmpleados();
                Empleado empleado = new Empleado();

                empleado = emp.consultarByNumeroEmpleado(1, txtnumero.Text);
                int estatus = empleado.Getstatus();
                if (estatus == 1)
                {
                    txtnombre.Text = empleado.Getnombre()+" " + empleado.Getapaterno()+" "+ empleado.Getamaterno();
                    txtnumero.Enabled = false;
                    gbrolcubrio.Visible = false;
                    int rol = empleado.Getrol();
                    int tipo = empleado.Gettipo();
                    switch (rol)
                    {
                        case 1:
                            rdbchofer.Checked = true;
                            break;
                        case 2:
                            rdbcargador.Checked = true;
                            break;
                        case 3:
                            rdbauxiliar.Checked = true;
                            break;

                    }
                    switch (tipo)
                    {
                        case 1:
                            rdbinterno.Checked = true;
                            break;
                        case 2:
                            rdbexterno.Checked = true;
                            break;

                    }
                }
                else
                {
                    MessageBox.Show("Empleado no existe ", "mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                }

            }
            else
            {
                v.solonumeros(e);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //metodo para grabar los movimientos de los empleados
            CLMovimientos mov = new CLMovimientos();
            MessageBox.Show(mov.grabar(txtnumero.Text,dtpfecha.Text, Convert.ToInt16( txtcantidad.Text), turno, rol_cubrio), "mensaje", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            txtnumero.Text = "";
            txtnumero.Enabled = true;
            txtnombre.Text = "";
            rdbchofer.Checked = false;
            rdbcargador.Checked = false;
            rdbauxiliar.Checked = false;
            rdbinterno.Checked = false;
            rdbexterno.Checked = false;
            txtcantidad.Text = "";
            checkturno.Checked = false;
            dtpfecha.ResetText();
            gbrolcubrio.Visible = false;
        }

        private void checkturno_CheckedChanged(object sender, EventArgs e)
        {
            turno = 1;
            gbrolcubrio.Visible = true;
            gbrolcubrio.Enabled = true;
            rdbchofercubrio.Enabled = true;
            rdbcargadorcubrio.Enabled = true;


        }

        private void FRMMovimientos_Load(object sender, EventArgs e)
        {
            gbrolcubrio.Visible = false;
        }

        private void rdbchofercubrio_CheckedChanged(object sender, EventArgs e)
        {
            rol_cubrio = 1;
        }

        private void rdbcargadorcubrio_CheckedChanged(object sender, EventArgs e)
        {
            rol_cubrio = 2;
        }

        private void gbrolcubrio_Enter(object sender, EventArgs e)
        {

        }
    }
}
