﻿namespace Vista.Formularios
{
    partial class FRMMovimientos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.dtpfecha = new System.Windows.Forms.DateTimePicker();
            this.button1 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtnumero = new System.Windows.Forms.TextBox();
            this.txtnombre = new System.Windows.Forms.TextBox();
            this.checkturno = new System.Windows.Forms.CheckBox();
            this.txtcantidad = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdbexterno = new System.Windows.Forms.RadioButton();
            this.rdbinterno = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdbauxiliar = new System.Windows.Forms.RadioButton();
            this.rdbcargador = new System.Windows.Forms.RadioButton();
            this.rdbchofer = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.rdbcargadorcubrio = new System.Windows.Forms.RadioButton();
            this.rdbchofercubrio = new System.Windows.Forms.RadioButton();
            this.gbrolcubrio = new System.Windows.Forms.GroupBox();
            this.panel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbrolcubrio.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.gbrolcubrio);
            this.panel1.Controls.Add(this.dtpfecha);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.txtnumero);
            this.panel1.Controls.Add(this.txtnombre);
            this.panel1.Controls.Add(this.checkturno);
            this.panel1.Controls.Add(this.txtcantidad);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.groupBox2);
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(21, 40);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(485, 336);
            this.panel1.TabIndex = 0;
            // 
            // dtpfecha
            // 
            this.dtpfecha.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpfecha.Location = new System.Drawing.Point(149, 204);
            this.dtpfecha.Name = "dtpfecha";
            this.dtpfecha.Size = new System.Drawing.Size(99, 20);
            this.dtpfecha.TabIndex = 18;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(148, 289);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 17;
            this.button1.Text = "Grabar";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(74, 147);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(28, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Tipo";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(76, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(26, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Rol:";
            // 
            // txtnumero
            // 
            this.txtnumero.Location = new System.Drawing.Point(149, 21);
            this.txtnumero.Name = "txtnumero";
            this.txtnumero.Size = new System.Drawing.Size(100, 20);
            this.txtnumero.TabIndex = 14;
            this.txtnumero.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtnumero_KeyPress);
            // 
            // txtnombre
            // 
            this.txtnombre.Enabled = false;
            this.txtnombre.Location = new System.Drawing.Point(149, 51);
            this.txtnombre.Name = "txtnombre";
            this.txtnombre.Size = new System.Drawing.Size(243, 20);
            this.txtnombre.TabIndex = 13;
            // 
            // checkturno
            // 
            this.checkturno.AutoSize = true;
            this.checkturno.Location = new System.Drawing.Point(270, 246);
            this.checkturno.Name = "checkturno";
            this.checkturno.Size = new System.Drawing.Size(83, 17);
            this.checkturno.TabIndex = 12;
            this.checkturno.Text = "Cubrió turno";
            this.checkturno.UseVisualStyleBackColor = true;
            this.checkturno.CheckedChanged += new System.EventHandler(this.checkturno_CheckedChanged);
            // 
            // txtcantidad
            // 
            this.txtcantidad.Location = new System.Drawing.Point(148, 244);
            this.txtcantidad.Name = "txtcantidad";
            this.txtcantidad.Size = new System.Drawing.Size(100, 20);
            this.txtcantidad.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(37, 247);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(111, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Cantidad de entregas:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(66, 204);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Fecha:";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox2.Controls.Add(this.rdbexterno);
            this.groupBox2.Controls.Add(this.rdbinterno);
            this.groupBox2.Location = new System.Drawing.Point(148, 147);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(282, 51);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            // 
            // rdbexterno
            // 
            this.rdbexterno.AutoSize = true;
            this.rdbexterno.Enabled = false;
            this.rdbexterno.Location = new System.Drawing.Point(82, 19);
            this.rdbexterno.Name = "rdbexterno";
            this.rdbexterno.Size = new System.Drawing.Size(61, 17);
            this.rdbexterno.TabIndex = 4;
            this.rdbexterno.TabStop = true;
            this.rdbexterno.Text = "Externo";
            this.rdbexterno.UseVisualStyleBackColor = true;
            // 
            // rdbinterno
            // 
            this.rdbinterno.AutoSize = true;
            this.rdbinterno.Enabled = false;
            this.rdbinterno.Location = new System.Drawing.Point(6, 19);
            this.rdbinterno.Name = "rdbinterno";
            this.rdbinterno.Size = new System.Drawing.Size(58, 17);
            this.rdbinterno.TabIndex = 3;
            this.rdbinterno.TabStop = true;
            this.rdbinterno.Text = "Interno";
            this.rdbinterno.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.groupBox1.Controls.Add(this.rdbauxiliar);
            this.groupBox1.Controls.Add(this.rdbcargador);
            this.groupBox1.Controls.Add(this.rdbchofer);
            this.groupBox1.Location = new System.Drawing.Point(148, 90);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(220, 51);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            // 
            // rdbauxiliar
            // 
            this.rdbauxiliar.AutoSize = true;
            this.rdbauxiliar.Enabled = false;
            this.rdbauxiliar.Location = new System.Drawing.Point(154, 19);
            this.rdbauxiliar.Name = "rdbauxiliar";
            this.rdbauxiliar.Size = new System.Drawing.Size(58, 17);
            this.rdbauxiliar.TabIndex = 2;
            this.rdbauxiliar.TabStop = true;
            this.rdbauxiliar.Text = "Auxiliar";
            this.rdbauxiliar.UseVisualStyleBackColor = true;
            // 
            // rdbcargador
            // 
            this.rdbcargador.AutoSize = true;
            this.rdbcargador.Enabled = false;
            this.rdbcargador.Location = new System.Drawing.Point(68, 19);
            this.rdbcargador.Name = "rdbcargador";
            this.rdbcargador.Size = new System.Drawing.Size(68, 17);
            this.rdbcargador.TabIndex = 1;
            this.rdbcargador.TabStop = true;
            this.rdbcargador.Text = "Cargador";
            this.rdbcargador.UseVisualStyleBackColor = true;
            // 
            // rdbchofer
            // 
            this.rdbchofer.AutoSize = true;
            this.rdbchofer.Enabled = false;
            this.rdbchofer.Location = new System.Drawing.Point(6, 19);
            this.rdbchofer.Name = "rdbchofer";
            this.rdbchofer.Size = new System.Drawing.Size(56, 17);
            this.rdbchofer.TabIndex = 0;
            this.rdbchofer.TabStop = true;
            this.rdbchofer.Text = "Chofer";
            this.rdbchofer.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(66, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nombre:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(35, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Numero de empleado:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(198, 17);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(133, 20);
            this.label7.TabIndex = 1;
            this.label7.Text = "MOVIMIENTOS";
            // 
            // rdbcargadorcubrio
            // 
            this.rdbcargadorcubrio.AutoSize = true;
            this.rdbcargadorcubrio.Enabled = false;
            this.rdbcargadorcubrio.Location = new System.Drawing.Point(79, 23);
            this.rdbcargadorcubrio.Name = "rdbcargadorcubrio";
            this.rdbcargadorcubrio.Size = new System.Drawing.Size(68, 17);
            this.rdbcargadorcubrio.TabIndex = 1;
            this.rdbcargadorcubrio.TabStop = true;
            this.rdbcargadorcubrio.Text = "Cargador";
            this.rdbcargadorcubrio.UseVisualStyleBackColor = true;
            this.rdbcargadorcubrio.CheckedChanged += new System.EventHandler(this.rdbcargadorcubrio_CheckedChanged);
            // 
            // rdbchofercubrio
            // 
            this.rdbchofercubrio.AutoSize = true;
            this.rdbchofercubrio.Enabled = false;
            this.rdbchofercubrio.Location = new System.Drawing.Point(17, 23);
            this.rdbchofercubrio.Name = "rdbchofercubrio";
            this.rdbchofercubrio.Size = new System.Drawing.Size(56, 17);
            this.rdbchofercubrio.TabIndex = 0;
            this.rdbchofercubrio.TabStop = true;
            this.rdbchofercubrio.Text = "Chofer";
            this.rdbchofercubrio.UseVisualStyleBackColor = true;
            this.rdbchofercubrio.CheckedChanged += new System.EventHandler(this.rdbchofercubrio_CheckedChanged);
            // 
            // gbrolcubrio
            // 
            this.gbrolcubrio.Controls.Add(this.rdbchofercubrio);
            this.gbrolcubrio.Controls.Add(this.rdbcargadorcubrio);
            this.gbrolcubrio.Location = new System.Drawing.Point(270, 269);
            this.gbrolcubrio.Name = "gbrolcubrio";
            this.gbrolcubrio.Size = new System.Drawing.Size(160, 54);
            this.gbrolcubrio.TabIndex = 19;
            this.gbrolcubrio.TabStop = false;
            this.gbrolcubrio.Enter += new System.EventHandler(this.gbrolcubrio_Enter);
            // 
            // FRMMovimientos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(535, 397);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.panel1);
            this.Name = "FRMMovimientos";
            this.Load += new System.EventHandler(this.FRMMovimientos_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbrolcubrio.ResumeLayout(false);
            this.gbrolcubrio.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdbcargador;
        private System.Windows.Forms.RadioButton rdbchofer;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtnumero;
        private System.Windows.Forms.TextBox txtnombre;
        private System.Windows.Forms.CheckBox checkturno;
        private System.Windows.Forms.TextBox txtcantidad;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdbexterno;
        private System.Windows.Forms.RadioButton rdbinterno;
        private System.Windows.Forms.RadioButton rdbauxiliar;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker dtpfecha;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RadioButton rdbcargadorcubrio;
        private System.Windows.Forms.RadioButton rdbchofercubrio;
        private System.Windows.Forms.GroupBox gbrolcubrio;
    }
}