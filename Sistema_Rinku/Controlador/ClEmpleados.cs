﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Modelo;


namespace Controlador
{  //clase controlador empelados
    public class ClEmpleados : IEmpleado<ClEmpleados>
    { 
        public Empleado consultarByNumeroEmpleado(int opcion,string numero_empleado)
        {
            MDEmpleados emp = new MDEmpleados();
            
            return emp.consultarByNumeroEmpleado(opcion, numero_empleado);
        }

        public ArrayList consultarEmpleados()
        {
            MDEmpleados emp = new MDEmpleados();
            ArrayList empleado = new ArrayList();
            empleado = emp.consultarEmpleados();
            return empleado;
        }

        public string eliminar(string numero_empleado)
        {
            MDEmpleados emp = new MDEmpleados();
            return emp.eliminar(numero_empleado);
        }

        public string grabar(Empleado obj)
        {
            MDEmpleados emp = new MDEmpleados();
            return emp.grabar(obj);
            throw new NotImplementedException();

        }
    }
}
