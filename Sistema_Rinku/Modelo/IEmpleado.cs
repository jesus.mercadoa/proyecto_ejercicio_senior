﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Modelo
{
    public interface IEmpleado<t>
    {
        // se crea interfaz para los empleados
        string grabar(Empleado obj);
        string eliminar(string numero_empleado);
        Empleado consultarByNumeroEmpleado(int opcion,string numero_empleado);
        ArrayList consultarEmpleados();
    }
}
