﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo
{    //se crea clasde modelo para los movimientos de colaboradores
    public class MDMovimientos : IMovimientos<MDMovimientos>
    {
        SqlCommand comando = new SqlCommand();
        ClConexion conexion = new ClConexion();
        SqlConnection con = new SqlConnection();


        public string grabar(string numero, string fecha, int cantidad, int turno, int rol_cubrio)
        {
            con.ConnectionString = conexion.Getmiconexion();
            comando.Connection = con;
            string mensaje = "";
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = "sp_Movimientos";
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@op", 1);
            comando.Parameters.AddWithValue("@numero", numero);
            comando.Parameters.AddWithValue("@fecha", fecha);
            comando.Parameters.AddWithValue("@cantidad", cantidad);
            comando.Parameters.AddWithValue("@turno", turno);
            comando.Parameters.AddWithValue("@rol_cubrio", rol_cubrio);

            try
            {
                con.Open();
                comando.ExecuteNonQuery();
                mensaje = "El Movimiento se ha registrado exitosamente";
            }
            catch
            {
                mensaje = " Error al registrar El movimiento";
            }
            finally
            {
                con.Close();
            }
            return mensaje;
        }
    }
}