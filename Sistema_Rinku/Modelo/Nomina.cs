﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo
{
    public class Nomina
    { // se crea clase para la nomina y sueldos
        private double sueldo_base1;
        private double pago_horas1;
        private double bonos1;
        private decimal isr1;
        private decimal vale1;
        private decimal suelo_mensual1;

        public decimal Getsuelo_mensual()
        {
            return suelo_mensual1;
        }

        public void Setsuelo_mensual(decimal value)
        {
            suelo_mensual1 = value;
        }

        public decimal Getvale()
        {
            return vale1;
        }

        public void Setvale(decimal value)
        {
            vale1 = value;
        }

        public decimal Getisr()
        {
            return isr1;
        }

        public void Setisr(decimal value)
        {
            isr1 = value;
        }

        public double Getbonos()
        {
            return bonos1;
        }

        public void Setbonos(double value)
        {
            bonos1 = value;
        }

        public double Getpago_horas()
        {
            return pago_horas1;
        }

        public void Setpago_horas(double value)
        {
            pago_horas1 = value;
        }

        public double Getsueldo_base()
        {
            return sueldo_base1;
        }

        public void Setsueldo_base(double value)
        {
            sueldo_base1 = value;
        }
    }
}
