﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Modelo
{
    public interface IMovimientos <t> 
    {
        //se crea interfaz para implementar metodo en los movimientos de loes empleados
        string grabar(string numero, string fecha, int cantidad, int turno,int rol_cubrio);
    }
}
