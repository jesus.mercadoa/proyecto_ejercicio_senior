﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo
{
   
    public class ClConexion
    { //se crea clase para hacer la concexion a base de datos
        private string miconexion1;

        public string Getmiconexion()
        {
            return miconexion1;
        }

        public void Setmiconexion(string value)
        {
            miconexion1 = value;
        }

        private string sentencia1;
        private SqlConnection conn;
        private SqlCommand cmd;

        public ClConexion()
        {
            Setmiconexion(@"Data Source= EQUIPOJESUSM;Initial Catalog=SistemaRinku;Integrated Security=True");
        }

        public ClConexion(string sentencia)
        {
            sentencia1 = sentencia;
            Setmiconexion(@"Data Source= EQUIPOJESUSM;Initial Catalog=SistemaRinku;Integrated Security=True");
        }
        public string Ejecutar()
        {

            conn = new SqlConnection(Getmiconexion());
            conn.Open();
            cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = sentencia1;
            cmd.ExecuteNonQuery();
            conn.Close();
            return "Operación exitosa";

        }
        public DataSet Consultar()
        {
            DataSet datos = new DataSet();
            conn = new SqlConnection(Getmiconexion());
            conn.Open();
            SqlDataAdapter resp = new SqlDataAdapter(sentencia1, conn);
            resp.Fill(datos, "Tabla");
            conn.Close();
            return datos;

        }

    }

}
