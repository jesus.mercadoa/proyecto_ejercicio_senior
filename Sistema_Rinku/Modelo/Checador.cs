﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo
{
    public class Checador
    {
        //se crea clase para el checador 
        private string fecha1;
        private string hora_entrada1;
        private string hora_salida1;
        private string numero1;

        public string Getnumero()
        {
            return numero1;
        }

        public void Setnumero(string value)
        {
            numero1 = value;
        }

        public string Gethora_salida()
        {
            return hora_salida1;
        }

        public void Sethora_salida(string value)
        {
            hora_salida1 = value;
        }

        public string Gethora_entrada()
        {
            return hora_entrada1;
        }

        public void Sethora_entrada(string value)
        {
            hora_entrada1 = value;
        }

        public string Getfecha()
        {
            return fecha1;
        }

        public void Setfecha(string value)
        {
            fecha1 = value;
        }
    }
}
