﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo
{
    public class MDNomina
    {
        //se crea modelo para la nomina
        SqlCommand comando = new SqlCommand();
        ClConexion conexion = new ClConexion();
        SqlConnection con = new SqlConnection();
        SqlDataReader rd;
        public Nomina consultarSueldo(string numero, DateTime fecha_ini,DateTime fecha_fin)
        {
            //metodo opara consultar bonos, sueldo al dia y pagos por entragas al cliente
            Nomina obj = new Nomina();
            con.ConnectionString = conexion.Getmiconexion();
            comando.Connection = con;
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = "sp_Nomina";
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@numero", numero);
            comando.Parameters.AddWithValue("@fecha_ini", fecha_ini);
            comando.Parameters.AddWithValue("@fecha_fin", fecha_fin);
            try
            {
                con.Open();
                rd = comando.ExecuteReader();
                while (rd.Read())
                {
                   
                    obj.Setsueldo_base(Convert.ToDouble(rd[0]));
                    obj.Setbonos(Convert.ToDouble(rd[1]));
                    obj.Setpago_horas(Convert.ToDouble(rd[2]));

                }


            }
            catch
            {
               
            }
            finally
            {
                con.Close();
            }
            return obj;
        }
    }
}
