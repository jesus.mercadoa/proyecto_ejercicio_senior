﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo
{
    public class Movimientos
    {
        //se crea clase para los movimeintos de los empleados
        private int id1;

        public int Getid()
        {
            return id1;
        }

        public void Setid(int value)
        {
            id1 = value;
        }

        private string fecha1;

        public string Getfecha()
        {
            return fecha1;
        }

        public void Setfecha(string value)
        {
            fecha1 = value;
        }

        private int cantidad1;

        public int Getcantidad()
        {
            return cantidad1;
        }

        public void Setcantidad(int value)
        {
            cantidad1 = value;
        }

        private int turno1;

        public int Getturno()
        {
            return turno1;
        }

        public void Setturno(int value)
        {
            turno1 = value;
        }

        private string numero1;

        public string Getnumero()
        {
            return numero1;
        }

        public void Setnumero(string value)
        {
            numero1 = value;
        }
    }
}
