﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo
{
    public class MDChecador : IChecador<MDChecador>
    {
        //metodo que realiza el registro de las entradas y salidas de jornada laboral
        SqlCommand comando = new SqlCommand();
        ClConexion conexion = new ClConexion();
        SqlConnection con = new SqlConnection();
        public int grabar(string numero, string fecha, string horae, int opcion)
        {
            con.ConnectionString = conexion.Getmiconexion();
            comando.Connection = con;
            int res;
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = "sp_checador";
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@op", opcion);
            comando.Parameters.AddWithValue("@numero", numero);
            comando.Parameters.AddWithValue("@fecha", fecha);
            comando.Parameters.AddWithValue("@horae", horae);
            comando.Parameters.AddWithValue("@horas", horae);


            try
            {
            con.Open();
                comando.ExecuteNonQuery();
                res = 1;
            }
            catch
            {
                res = 0;
            }
            finally
            {
                con.Close();
            }
            return res;
        }
    }
}
