﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Modelo
{
    public class Empleado 
    {
        //se crea  clase para los empleados
        private string numero1;
        private string nombre1;
        private string apaterno1;
        private string amaterno1;
        private int rol1;
        private int tipo1;
        private int status1;
        private int opcion1;

        public int Getopcion()
        {
            return opcion1;
        }

        public void Setopcion(int value)
        {
            opcion1 = value;
        }

        public string Getnumero()
        {
            return numero1;
        }

        public void Setnumero(string value)
        {
            numero1 = value;
        }

       

        public string Getnombre()
        {
            return nombre1;
        }

        public void Setnombre(string value)
        {
            nombre1 = value;
        }

        

        public string Getapaterno()
        {
            return apaterno1;
        }

        public void Setapaterno(string value)
        {
            apaterno1 = value;
        }

        

        public string Getamaterno()
        {
            return amaterno1;
        }

        public void Setamaterno(string value)
        {
            amaterno1 = value;
        }

        

        public int Getrol()
        {
            return rol1;
        }

        public void Setrol(int value)
        {
            rol1 = value;
        }

       

        public int Gettipo()
        {
            return tipo1;
        }

        public void Settipo(int value)
        {
            tipo1 = value;
        }

        

        public int Getstatus()
        {
            return status1;
        }

        public void Setstatus(int value)
        {
            status1 = value;
        }
    }
}
