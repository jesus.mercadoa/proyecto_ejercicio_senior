﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace Modelo
{
    public class MDEmpleados : IEmpleado<MDEmpleados>
    {
        SqlCommand comando = new SqlCommand();
        ClConexion conexion = new ClConexion();
        SqlConnection con = new SqlConnection();
        SqlDataReader rd;
        public Empleado consultarByNumeroEmpleado(int opcion,string numero_empleado)
        {
            //metodo para consultar un empelado por numero
            int res = 0;
            Empleado obj = new Empleado();
            con.ConnectionString = conexion.Getmiconexion();
            comando.Connection = con;
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = "sp_Empleados";
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@op", opcion);
            comando.Parameters.AddWithValue("@numero", numero_empleado);
            try
            {
                con.Open();
                rd = comando.ExecuteReader();
                while (rd.Read())
                {
                    obj.Setnombre(rd[0].ToString());
                    obj.Setapaterno(rd[1].ToString());
                    obj.Setamaterno(rd[2].ToString());
                    obj.Setrol(Convert.ToInt16(rd[3]));
                    obj.Settipo(Convert.ToInt16(rd[4]));
                    obj.Setstatus(Convert.ToInt16(rd[5]));
                    res = 1;
                }

                
            }
            catch
            {
                res = 0;
            }
            finally
            {
                con.Close();
            }
            return obj;
        }

        public ArrayList consultarEmpleados()
        {
            //metodo para consultar todo los empleados
            int res = 0;
            List<Empleado> lista = new List<Empleado>();
            ArrayList list = new ArrayList();
            con.ConnectionString = conexion.Getmiconexion();
            comando.Connection = con;
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = "sp_Empleados";
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@op",4);
            try
            {
                con.Open();
                rd = comando.ExecuteReader();
                while (rd.Read())
                {
                    Empleado obj = new Empleado();
                    obj.Setnumero(rd[0].ToString());
                    obj.Setnombre(rd[1].ToString());
                    obj.Setapaterno(rd[2].ToString());
                    obj.Setamaterno(rd[3].ToString());
                    obj.Setrol(Convert.ToInt16(rd[4]));
                    obj.Settipo(Convert.ToInt16(rd[5]));
                    list.Add(obj);
                    
                }


           }
           catch
           {
                res = 0;
            }
           finally
           {
                con.Close();
            }
            return list;
        }

        public string eliminar(string numero_empleado)
        {
            //metodo para eliminar un mepleado
            string mensaje = "";
            con.ConnectionString = conexion.Getmiconexion();
            comando.Connection = con;
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = "sp_Empleados";
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@op", 3);
            comando.Parameters.AddWithValue("@numero", numero_empleado);
            try
            {
                con.Open();
                comando.ExecuteNonQuery();
                mensaje = "El Empleado se ha eliminado exitosamente";
            }
            catch
            {
                mensaje = " Error al eliminar Empleado";
            }
            finally
            {
                con.Close();
            }
            return mensaje;
        }

        public string grabar(Empleado obj)
        {
            //metodo para grabar un empleado
            con.ConnectionString = conexion.Getmiconexion();
            comando.Connection = con;
            string mensaje = "";
            comando.CommandType = CommandType.StoredProcedure;
            comando.CommandText = "sp_Empleados";
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@op", obj.Getopcion());
            comando.Parameters.AddWithValue("@numero", obj.Getnumero());
            comando.Parameters.AddWithValue("@nombre", obj.Getnombre());
            comando.Parameters.AddWithValue("@apaterno", obj.Getapaterno());
            comando.Parameters.AddWithValue("@amaterno", obj.Getamaterno());
            comando.Parameters.AddWithValue("@rol",obj.Getrol());
            comando.Parameters.AddWithValue("@tipo", obj.Gettipo());
            comando.Parameters.AddWithValue("@estatus", obj.Getstatus());
         
            try
            {
                con.Open();
                comando.ExecuteNonQuery();
                mensaje = "El Empleado se ha registrado exitosamente";
            }
            catch
            {
                mensaje = " Error al registrar El empleado";
            }
            finally
            {
                con.Close();
            }
            return mensaje;
        }
    }
}
